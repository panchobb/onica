<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="author" content="Fran Blanco, yo@panchoblanco.com">

  <meta name="keywords" content="ropa, moda, onica, fashon, lo mejor" />
  <!-- <meta name="description" content="150 words"/> -->
  <meta name="subject" content="ropa">
  <meta name="copyright" content="Onica">
  <meta name="language" content="ES">

  <meta name="revised" content="Jueves 23, Agosto, 2018" />

  <meta name="Classification" content="Business">
  <meta name="designer" content="">
  <meta name="copyright" content="">
  <meta name="reply-to" content="email@hotmail.com">
  <meta name="owner" content="">
  <meta name="url" content="http://www.onica.com.ar">
  <meta name="identifier-URL" content="http://www.onica.com.ar">

  <meta http-equiv="cleartype" content="on">
  <meta name="MobileOptimized" content="320">
  <meta name="theme-color" content="#172da9">
  <meta name="HandheldFriendly" content="True">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Onica</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <style>
    .placeholder {
      background-image:url(img/bg-<?= rand(0, 1) === 1 ? 1 : 2 ?>.jpg);
    }
    @media screen and (min-width: 768px){
      .placeholder {
        background-image:url(img/bg-3.jpg);
      }
    }
  </style>
</head>

<body>
  <section class="placeholder">
    <h3><a href="https://onica.mitiendanube.com/">Shop Online</a></h3>
    <h1>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 647.077 204.866">
        <defs>
          <style>
            .a {
              fill: #172da9;
            }
          </style>
        </defs>
        <g transform="translate(0 67.344)">
          <path class="a" d="M128.073,201.8a32.268,32.268,0,0,0-13.727,2.835c-.249.1-.5.249-.746.348a35.1,35.1,0,0,1,14.623-3.183Z"
            transform="translate(-57.099 -168.775)" />
          <path class="a" d="M143.378,342.837a33.231,33.231,0,0,0,13.131-2.537,36.08,36.08,0,0,1-13.131,2.537Z" transform="translate(-72.055 -238.389)"
          />
          <path class="a" d="M137.075,177.129a68.017,68.017,0,0,0-14.822-21.735,69.726,69.726,0,0,0-22.481-14.623,73.787,73.787,0,0,0-28.35-5.372,73.787,73.787,0,0,0-28.35,5.372,71.137,71.137,0,0,0-22.68,14.722A67.405,67.405,0,0,0,0,204.136v.4a66.669,66.669,0,0,0,5.372,26.659,68.017,68.017,0,0,0,14.822,21.735A70.665,70.665,0,0,0,42.724,267.5a73.786,73.786,0,0,0,28.35,5.372,73.786,73.786,0,0,0,28.35-5.372,71.138,71.138,0,0,0,22.68-14.722,66.551,66.551,0,0,0,15.021-21.934,67.322,67.322,0,0,0,5.372-26.758v-.4A67.262,67.262,0,0,0,137.075,177.129ZM85.6,236.813c-.149.05-.3.149-.448.2-.249.1-.448.2-.7.3a33.231,33.231,0,0,1-13.131,2.537h-.1A35.707,35.707,0,0,1,56.5,171.608c.249-.1.5-.249.746-.348a32.428,32.428,0,0,1,13.727-2.835h.3A35.69,35.69,0,0,1,85.6,236.813Z"
            transform="translate(0 -135.4)" />
          <path class="a" d="M170.5,339.5c.149-.05.3-.149.448-.2-.4.149-.746.348-1.144.5C170.049,339.7,170.3,339.6,170.5,339.5Z"
            transform="translate(-85.347 -237.887)" />
        </g>
        <path class="a" d="M111.587,0h30.936q-6.118,26.858-24.073,41.033c-11.937,9.45-25.167,14.125-39.69,14.125q-21.412,0-39.342-13.528Q21.513,28.126,14.6,0H45.536c8.057,16.612,19.2,24.968,33.373,24.968C93.234,24.968,104.126,16.612,111.587,0Z"
          transform="translate(-7.338)" />
        <path class="a" d="M327.2,140.6h34.269l54.512,69.93V140.6h36.109V272.85h-32.18l-56.6-72.516V272.9H327.2V140.6Z" transform="translate(-164.461 -70.67)"
        />
        <path class="a" d="M629,140.6h36.855V272.85H629Z" transform="translate(-316.155 -70.67)" />
        <path class="a" d="M812.039,272.872a69.162,69.162,0,0,1-26.659-5.123,64.846,64.846,0,0,1-21.636-14.274A68.559,68.559,0,0,1,743.8,204.434v-.4a68.84,68.84,0,0,1,19.845-48.842,66.676,66.676,0,0,1,22.033-14.623,71.973,71.973,0,0,1,27.853-5.272,78.45,78.45,0,0,1,18.5,1.989,65.941,65.941,0,0,1,15.219,5.67,60.066,60.066,0,0,1,12.484,8.8,69.936,69.936,0,0,1,9.9,11.141l-27.6,21.337a48.338,48.338,0,0,0-12.484-11.34c-4.526-2.785-10-4.178-16.463-4.178a28.951,28.951,0,0,0-12.733,2.736,30.593,30.593,0,0,0-9.9,7.461,34.816,34.816,0,0,0-6.516,11.141,38.666,38.666,0,0,0-2.338,13.628v.4a41.047,41.047,0,0,0,2.338,13.877,34.775,34.775,0,0,0,6.516,11.241,31.134,31.134,0,0,0,9.9,7.461,29.105,29.105,0,0,0,12.733,2.736,36.761,36.761,0,0,0,9.351-1.144,28.48,28.48,0,0,0,7.759-3.283,40,40,0,0,0,6.615-5.123,70.3,70.3,0,0,0,6.118-6.516l27.6,19.646a94.432,94.432,0,0,1-10.3,11.887,60.159,60.159,0,0,1-12.683,9.45,65.519,65.519,0,0,1-15.866,6.217A76.283,76.283,0,0,1,812.039,272.872Z"
          transform="translate(-373.857 -68.006)" />
        <path class="a" d="M1059.8,138.7h35.313l56.352,133.2h-39.292l-9.649-23.625H1051.5l-9.45,23.625H1003.5Zm32.13,81.27-14.722-37.6-14.921,37.6Z"
          transform="translate(-504.391 -69.715)" />
      </svg>
    </h1>
    <footer>
      <a href="https://www.facebook.com/somos.onica/" data-social="facebook">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.852 44.917">
          <path class="social_icon" d="M20.03,22.5H13.752V44.917H4.41V22.5H0V14.574H4.41V9.417C4.41,5.755,6.2,0,13.826,0H20.7V7.7H15.695a1.9,1.9,0,0,0-1.943,2.167V14.5h7.1Zm0,0"/>
        </svg>
      </a>
      <a href="https://www.instagram.com/somos.onica/" data-social="instagram">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.422 45.422">
          <g transform="translate(-287.4)">
            <path class="social_icon" d="M320.276,0h-20.33A12.563,12.563,0,0,0,287.4,12.546v20.33a12.563,12.563,0,0,0,12.546,12.546h20.33a12.563,12.563,0,0,0,12.546-12.546V12.546A12.563,12.563,0,0,0,320.276,0Zm8.465,32.876a8.531,8.531,0,0,1-8.54,8.54H299.87a8.531,8.531,0,0,1-8.54-8.54V12.546a8.531,8.531,0,0,1,8.54-8.54H320.2a8.531,8.531,0,0,1,8.54,8.54Zm0,0"/>
            <path class="social_icon" d="M313.614,14.6a11.714,11.714,0,1,0,11.714,11.714A11.724,11.724,0,0,0,313.614,14.6Zm0,19.348a7.709,7.709,0,1,1,7.709-7.709,7.741,7.741,0,0,1-7.709,7.709Zm0,0" transform="translate(-3.541 -3.566)" />
            <path class="social_icon" d="M332.547,10a3.248,3.248,0,0,0-2.116.831,2.969,2.969,0,0,0-.831,2.116,3.248,3.248,0,0,0,.831,2.116,2.969,2.969,0,0,0,2.116.831,3.248,3.248,0,0,0,2.116-.831,2.969,2.969,0,0,0,.831-2.116,3.248,3.248,0,0,0-.831-2.116A3.331,3.331,0,0,0,332.547,10Zm0,0" transform="translate(-10.307 -2.442)" />
          </g>
        </svg>
      </a>
    </footer>
  </section>
</body>

</html>