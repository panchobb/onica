<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="css/helpers.css">
  <!-- development version, includes helpful console warnings -->
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
  <header>

  </header>
  <section>
    <img class='img--mini' src="imgs/chica-tenis-piso@2x.png" alt="">
    <!-- <?= $fuente_1 ?> -->
    <img class='img--mini' src="imgs/chica-tenis-2@2x.png" alt="">
     <!-- <?= $fuente_2 ?> -->
    <img class='img--mini' src="imgs/chica-arrodillada@2x.png" alt="">
     <!-- <?= $fuente_3 ?> -->
    <img class='img--mini' src="imgs/chica-mirando-derecha@2x.png" alt="">
     <!-- <?= $fuente_4 ?> -->
    <img class='img--mini' src="imgs/chica-transparente@2x.png" alt="">
     <!-- <?= $fuente_5 ?> -->
    <img class='img--mini' src="imgs/cartel@2x.png" alt="">
     <!-- <?= $fuente_6 ?> -->
    <img class='img--mini' src="imgs/chica-mirando-izq@2x.png" alt="">
     <!-- <?= $fuente_7 ?> -->
    <img class='img--mini' src="imgs/chica-acostada@2x.png" alt="">
     <!-- <?= $fuente_8 ?> -->
    <img class='img--mini' src="imgs/chica-silla@2x.png" alt="">
     <!-- <?= $fuente_9 ?> -->
    <img class='img--mini' src="imgs/cartel-invert@2x.png" alt="">
     <!-- <?= $fuente_10 ?> -->
    <img class='img--mini' src="imgs/rect@2x.png" alt="">
     <!-- <?= $fuente_11 ?> -->
    <img class='img--mini' src="imgs/nipple@2x.png" alt="">
     <!-- <?= $fuente_12 ?> -->
    <img class='img--mini' src="imgs/net@2x.png" alt="">
     <!-- <?= $fuente_13 ?> -->
    <img class='img--mini' src="imgs/standing@2x.png" alt="">
     <!-- <?= $fuente_14 ?> -->
    <img class='img--mini' src="imgs/standing3@2x.png" alt="">
     <!-- <?= $fuente_15 ?> -->
    <img class='img--mini' src="imgs/standing2@2x.png" alt="">
     <!-- <?= $fuente_16 ?> -->
    <img class='img--mini' src="imgs/cartel.png" alt="">
     <!-- <?= $fuente_17 ?> -->
  </section>
  <main id='app'>

  </main>
  <footer>

  </footer>
</body>
</html>



<script>
  new Vue({
    el: '#app',
    data: {

    },
    methods:{

    }
  })
</script>