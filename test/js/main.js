window.addEventListener('DOMContentLoaded', () => {
    // console.log(window.location.pathname);
    
    document.querySelectorAll('.menu-ham a').forEach((e) => {
        
        if (window.location.href === e.href) {
            e.classList.add('selected');      
        } 
    })

    if (window.location.pathname === '/') {
        document.querySelector('.menu-ham a').classList.add('selected');
    }
    
    const noscroll = () => {
        window.scrollTo(0, 0);
    }
    
    const showNav = () => {
        document.body.style.overflow = 'hidden'; // Oculta la scrollbar    
        window.addEventListener('scroll', noscroll); // Deshabilita scroll
        document.querySelector('.menu-mobile').style.display = 'flex';
        document.querySelector('.menu-mobile').style.position = 'absolute';
    }
    
    const hideNav = () => {
        window.removeEventListener('scroll', noscroll); // Habilita scroll
        document.body.style.overflow = ''; // Muestra la scrollbar
        document.querySelector('.menu-mobile').style.display = 'none';
        document.querySelector('.menu-mobile').style.position = '';
    }

    document.querySelectorAll('.nav-cruz').forEach((e) => {e.addEventListener('click', hideNav);});

    document.querySelector('.menu-hamburguer').addEventListener('click', showNav)
});