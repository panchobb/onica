<?php
require_once 'basededatos.php';
require_once 'helpers.php';

if($_POST){

  $nota1 = [
    'foto' => ifEmpty($_FILES['foto1'],$_POST['fallbackIMG1'],'evento1'),
    'titulo' => trim($_POST['titulo1']),
    'texto' => trim($_POST['texto1']),
    'fecha' => trim($_POST['fecha1']),
    'link' => trim($_POST['link1'])
  ];
  $nota2 = [
    'foto' => ifEmpty($_FILES['foto2'],$_POST['fallbackIMG2'], 'evento2'),
    'titulo' => trim($_POST['titulo2']),
    'texto' => trim($_POST['texto2']),
    'fecha' => trim($_POST['fecha2']),
    'link' => trim($_POST['link2'])
  ];
  $nota3 = [
    'foto' => ifEmpty($_FILES['foto3'],$_POST['fallbackIMG3'], 'evento3'),
    'titulo' => trim($_POST['titulo3']),
    'texto' => trim($_POST['texto3']),
    'fecha' => trim($_POST['fecha3']),
    'link' => trim($_POST['link3'])
  ];

  //dd("Hola Notas",$nota1, $nota2, $nota3);

  guardar($nota1,true);
  guardar($nota2);
  guardar($nota3);

  //dd(traer());
  redirect('../index.php');
}
