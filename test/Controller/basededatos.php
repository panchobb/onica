<?php
/* GUARDAR IMGs */
function guardarIMG($ruta, $borrar = false)
{
  $eleJson = json_encode($ruta);
  if ($borrar === true) {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', $eleJson . PHP_EOL);
  } else {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', $eleJson . PHP_EOL, FILE_APPEND);
  }
}

function traerIMGs()
{
  $arrayCosas = [];
  $archivo = fopen(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', 'r');
  while (($linea = fgets($archivo)) !== false) {
    if ($linea !== '') {
      $arrayCosas[] = json_decode($linea, true);
    }
  }
  fclose($archivo);
  $array = $arrayCosas;

  return $array;
}